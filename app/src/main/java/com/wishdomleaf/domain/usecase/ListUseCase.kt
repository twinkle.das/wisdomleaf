package com.wishdomleaf.domain.usecase

import com.wishdomleaf.data.repository.ListRepository
import com.wishdomleaf.domain.base.UseCase
import com.wishdomleaf.domain.entities.ListWrapper
import com.wishdomleaf.domain.entities.Result

class ListUseCase : UseCase<ListWrapper>() {

    private val repository by lazy { ListRepository() }

    override suspend fun makeRequest(): Result<ListWrapper> =
        repository.getListData()


    fun fetchData(success: (ListWrapper) -> Unit, failure: (Exception) -> Unit) {
        execute(success, failure)
    }
}