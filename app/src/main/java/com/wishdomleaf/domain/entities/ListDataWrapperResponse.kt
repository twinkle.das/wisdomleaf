package com.wishdomleaf.domain.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

class ListDataWrapperResponse (
    @SerializedName("id") val id:String,
    @SerializedName("author") val author:String,
    @SerializedName("width") val width:Long,
    @SerializedName("height") val height:Long,
    @SerializedName("url") val url:String,
    @SerializedName("download_url") val download_url:String,
    ){
    fun toListWrapper() = ListWrapper(id,author,width,height,url,download_url)
}

@Parcelize
data class ListWrapper(
    val id:String,
    val author:String,
    val width:Long,
    val height:Long,
    val url:String,
    val download_url:String,

    ): Parcelable
