package com.wishdomleaf.domain.exceptions

class ApiException(
    error: String?
) : Exception(error)