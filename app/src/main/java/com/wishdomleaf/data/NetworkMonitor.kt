package com.wishdomleaf.data

interface NetworkMonitor {
    fun isConnected(): Boolean
}