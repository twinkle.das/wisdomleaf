package com.wishdomleaf.data.repository

import com.wishdomleaf.data.repository.base.BaseApiRepository

class ListRepository : BaseApiRepository() {

    suspend fun getListData() =
        parseResult(api.getList()) { response -> response.toListWrapper() }
}