package com.wishdomleaf.data.retrofit

import com.wishdomleaf.domain.entities.*
import retrofit2.Response
import retrofit2.http.Url

interface ApiService {

    companion object {
        const val GET_LIST = "list?page=2&limit=40"
    }

    suspend fun getList(@Url url:String ="https://jarvis.mynationvoice.com/bangla/stories?slug=livetv"): Response<ListDataWrapperResponse>
}