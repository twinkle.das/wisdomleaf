package com.wishdomleaf.presentation

import android.app.Application
import android.content.Context

class WisdomLeafApplication : Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: WisdomLeafApplication? = null

        fun applicationContext(): Context {
            return instance?.applicationContext!!
        }
    }
}