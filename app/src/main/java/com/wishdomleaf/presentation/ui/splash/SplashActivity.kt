package com.wishdomleaf.presentation.ui.splash

import android.content.Intent
import android.os.Bundle
import com.wisdomleaf.R
import com.wisdomleaf.databinding.ActivitySplashBinding
import com.wishdomleaf.presentation.base.BaseActivity
import com.wishdomleaf.presentation.model.ViewModelCreator
import com.wishdomleaf.presentation.ui.activity.MainActivity
import java.util.*


class SplashActivity : BaseActivity<SplashActivityViewModel, ActivitySplashBinding>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timer().schedule(object : TimerTask() {
            override fun run() {
                val intent = Intent(this@SplashActivity, MainActivity::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.`in`, R.anim.out)
                finish()
            }
        }, 5000)
    }

    override fun subscribeToObservers() {
    }

    override fun createViewModel(): ViewModelCreator<SplashActivityViewModel> =
        ViewModelCreator(SplashActivityViewModel::class.java)

    override fun getViewBinding(): ActivitySplashBinding = ActivitySplashBinding.inflate(layoutInflater)

}