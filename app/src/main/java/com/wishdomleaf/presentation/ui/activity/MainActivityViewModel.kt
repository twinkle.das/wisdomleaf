package com.wishdomleaf.presentation.ui.activity

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.wishdomleaf.domain.entities.ListWrapper
import com.wishdomleaf.domain.usecase.ListUseCase

class MainActivityViewModel : ViewModel() {
    private val userUserCase by lazy { ListUseCase() }
    val listData = MutableLiveData<ListWrapper>()
    val onError = MutableLiveData<Boolean>()
    fun getListData() {
        userUserCase.fetchData(::onApiSuccess, ::onApiFailure)
    }

    private fun onApiSuccess(temp: ListWrapper) {
        listData.value = temp
    }

    private fun onApiFailure(exception: Exception) {
        onError.value = true
    }

}