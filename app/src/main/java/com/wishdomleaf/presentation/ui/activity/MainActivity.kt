package com.wishdomleaf.presentation.ui.activity

import android.os.Bundle
import com.wishdomleaf.presentation.base.BaseActivity
import com.wishdomleaf.presentation.model.ViewModelCreator
import com.wisdomleaf.databinding.MainActivityBinding


class MainActivity : BaseActivity<MainActivityViewModel, MainActivityBinding>() {
    override fun subscribeToObservers() {
        viewModel.apply {

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getListData()
    }

    override fun createViewModel(): ViewModelCreator<MainActivityViewModel> =
        ViewModelCreator(MainActivityViewModel::class.java)

    override fun getViewBinding(): MainActivityBinding = MainActivityBinding.inflate(layoutInflater)
}