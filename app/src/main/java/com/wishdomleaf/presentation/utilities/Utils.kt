package com.wishdomleaf.presentation.utilities

import java.text.SimpleDateFormat
import java.util.*

class Utils {
    companion object {

        fun convertKelvinToCelsius(kelvin: Float): Int {
            return (kelvin - 273.15).toInt()
        }

        fun getDayOfTheWeek(timeStamp: Long): String {
            val sdf = SimpleDateFormat("EEEE")
            val netDate = Date(timeStamp * 1000)
            return sdf.format(netDate)
        }

        fun getHours(timeStamp: Long): String {
            val sdf =SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa")
            val date = sdf.format(Date(timeStamp * 1000))
            val outputFormat =  SimpleDateFormat("HH")
            return  outputFormat.format(sdf.parse(date))
        }



        fun getCurrentMatchingHours(): String {
            val rightNow = Calendar.getInstance()
            val currentTime = rightNow[Calendar.HOUR_OF_DAY]
            var nearByTime = "23"
            when (currentTime) {
                in 0..4 -> nearByTime = "02"
                in 4..6 -> nearByTime = "05"
                in 6..9 -> nearByTime = "08"
                in 9..12 -> nearByTime = "11"
                in 12..15 -> nearByTime = "14"
                in 15..18 -> nearByTime = "17"
                in 18..21 -> nearByTime = "20"
            }
            return nearByTime
        }
    }
}