package com.wishdomleaf.presentation.utilities

import android.content.Context
import android.net.ConnectivityManager
import com.wishdomleaf.data.NetworkMonitor
import com.wishdomleaf.presentation.WisdomLeafApplication


class LiveConnectivityMonitor : NetworkMonitor {

    override fun isConnected(): Boolean {
        val cm =
            WisdomLeafApplication.applicationContext().getSystemService(Context.CONNECTIVITY_SERVICE)
                    as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnected
    }
}