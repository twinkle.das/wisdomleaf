package com.wishdomleaf.presentation.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
class List (
    val id: String,
    val author: String,
    val width: Long,
    val height: Long,
    val url: String,
    val download_url: String
) : Parcelable